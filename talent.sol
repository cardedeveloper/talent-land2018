pragma solidity 0.4.21;

contract helloWorld {
    
    uint qty;

    struct computer {
        uint256 price;
        uint256 year;
        string model;
        string brand; 
    }
    computer[] computers;

    function helloWorld( uint256 _val) public{
        qty = _val;
    }
    function getQty() public constant returns (uint256){
        return qty;
    }
    function addComputer(uint256 _price, uint256 _year, string _model, string _brand)public {
        qty += 1;
        computer memory comp;
        comp.price = _price;
        comp.year = _year;
        comp.model = _model;
        comp.brand = _brand;
        computers.push(comp);
    }

    function getPrice(uint256 _index) public constant returns(uint256){
        return computers[_index].price;
    }

    function getYear(uint256 _index) public constant returns(uint256){
        return computers[_index].year;
    }
    function getModel(uint256 _index) public constant returns(string){
        return computers[_index].model;
    }

    function getBrand(uint256 _index) public constant returns(string){
        return computers[_index].brand;
    }
}